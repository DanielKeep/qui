use futures::{Async, Future, Poll};
use futures::stream::Stream;
use ::Error;
use ::gui::Gui;
use ::window::{MouseButton, MouseEvent, MouseEventArgs};

pub trait FutureTaskExt: Sized {
    fn spawn_task(self);
}

impl<F> FutureTaskExt for F
where F: 'static + Future<Item=(), Error=Error> {
    fn spawn_task(self) {
        Gui::with_mut(|gui| gui.spawn_task(self))
    }
}

pub trait MouseEventStreamExt: Sized {
    fn clicked(self, button: MouseButton) -> MouseClickStream<Self>;
}

impl<S> MouseEventStreamExt for S
where S: Stream<Item=MouseEventArgs> {
    fn clicked(self, button: MouseButton) -> MouseClickStream<Self> {
        MouseClickStream {
            in_: self,
            button: button,
            is_down: false,
        }
    }
}

pub struct MouseClickStream<S> {
    in_: S,
    button: MouseButton,
    is_down: bool,
}

impl<S> Stream for MouseClickStream<S>
where S: Stream<Item=MouseEventArgs> {
    type Item = MouseEventArgs;
    type Error = S::Error;

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        loop {
            // Poll repeatedly until we run out of events, or we find a complete click.
            let me = try_ready!(self.in_.poll());

            let me = match me {
                Some(me) => me,
                None => {
                    return Ok(Async::Ready(None))
                },
            };


            match me.event {
                MouseEvent::Down(b)
                | MouseEvent::DoubleDown(b)
                if b == self.button => {
                    self.is_down = true;
                },
                MouseEvent::Up(b) if b == self.button => {
                    let clicked = self.is_down;
                    self.is_down = false;
                    if clicked {
                        return Ok(Async::Ready(Some(me)));
                    }
                },

                MouseEvent::Down(_)
                | MouseEvent::DoubleDown(_)
                | MouseEvent::Up(_)
                | MouseEvent::Move => {}
            }
        }
    }
}
