use ::util::Alive;

macro_rules! boolish {
    (@as_items $($i:item)*) => { $($i)* };

    (@convs $name:ident, $yes:ident, $no:ident) => {
        impl From<bool> for $name {
            fn from(v: bool) -> Self {
                match v {
                    true => $name::$yes,
                    false => $name::$no,
                }
            }
        }

        impl From<$name> for bool {
            fn from(v: $name) -> Self {
                match v {
                    $name::$yes => true,
                    $name::$no => false,
                }
            }
        }
    };

    (
        $(#[$($attrs:tt)*])*
        pub boolish $name:ident {
            $yes:ident = true,
            $no:ident = false $(,)*
        }
    ) => {
        boolish! {
            @as_items
            $(#[$($attrs)*])*
            pub enum $name {
                $yes,
                $no
            }

            boolish! { @convs $name, $yes, $no }
        }
    };

    (
        $(#[$($attrs:tt)*])*
        boolish $name:ident {
            $yes:ident = true,
            $no:ident = false $(,)*
        }
    ) => {
        boolish! {
            @as_items
            $(#[$($attrs)*])*
            enum $name {
                $yes,
                $no
            }

            boolish! { @convs $name, $yes, $no }
        }
    };
}

macro_rules! rethrow {
    ($e:expr) => {
        return match $e {
            ::std::result::Result::Ok(v) => ::std::result::Result::Ok(v),
            ::std::result::Result::Err(err) => ::std::result::Result::Err(::std::convert::From::from(err)),
        }
    };
}

pub trait GuiInternal {
    fn assert_is_running();
    fn assert_not_running();

    fn with<F, R>(f: F) -> R where F: FnOnce(&Self) -> R;
    fn with_mut<F, R>(f: F) -> R where F: FnOnce(&mut Self) -> R;

    // fn register_dialog_window(&mut self, window: &Window);
    fn register_keep_alive<T: 'static + Alive>(&mut self, keep_alive: T);
}
