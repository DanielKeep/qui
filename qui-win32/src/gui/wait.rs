/*!
Wait future implementation.

Well, *some* of it.  Most of the actual waiting stuff is tied to the event loop itself, so `Event`, `WakeInstant`, and the event heap are in `Gui`.
*/
use std::time::{Duration, Instant};
use futures::{Async, Future, Poll};
use ::Error;
use super::{Event, EventSlot, Gui, SlabExt, WakeInstant};

/**
Future that completes after waiting a certain length of time.
*/
pub struct Wait {
    instant: Instant,
    event: Option<EventSlot>,
}

impl Wait {
    pub fn new(dur: Duration) -> Wait {
        trace!("Wait::new({:?})", dur);
        let ins = Instant::now() + dur;
        trace!("- wake at: {:?}", ins);
        Gui::with_mut(|gui| {
            // Ensure we have enough space in the many varied things into which we place other things.
            gui.delay_wake_instants.reserve(1);
            let e_entry = gui.events.some_vacant_entry();

            // Add the event.
            let event_slot = e_entry.index();
            gui.delay_wake_instants.push(WakeInstant {
                instant: ins,
                event: event_slot,
            });
            e_entry.insert(Event::Pending);

            Wait {
                instant: ins,
                event: Some(event_slot),
            }
        })
    }

    pub fn instant(&self) -> Instant {
        self.instant
    }
}

impl Drop for Wait {
    fn drop(&mut self) {
        trace!("Wait::drop()");
        /*
        TODO: Remove wait instant.

        What we *should* do here is remove the this wait from the heap and wake event slab... but we *can't* because `libstd`'s `BinaryHeap` doesn't have a `remove` method.

        For now, we'll just ignore this, and allow it to expire "naturally".
        */

        // Take care of the event.
        if let Some(event) = self.event {
            Gui::with_mut(|gui| gui.clean_up_event(event));
        }
    }
}

impl Future for Wait {
    type Item = Duration;
    type Error = Error;

    fn poll(&mut self) -> Poll<Duration, Error> {
        trace!("Wait::poll()");
        let now = Instant::now();
        if self.instant <= now {
            Ok(Async::Ready(now.duration_since(self.instant)))
        } else if let Some(event) = self.event {
            Gui::with_mut(|gui| gui.block_on_event(event)
                .map(|async| async
                    .map(|()| now.duration_since(self.instant))))
        } else {
            panic!("Wait.event is empty, yet timeout hasn't been reached");
        }
    }
}
