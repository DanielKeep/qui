use std::cell::RefCell;
use std::rc::Rc;
use std::time::{Duration};
use futures::{Async, Future, Poll};
use futures::stream::Stream;
use ::Error;
use super::{EventSlot, Gui};
use super::wait::Wait;

pub struct SetCountdown<T> {
    inner: Rc<RefCell<Inner<T>>>,
}

pub struct CountdownDone<T> {
    inner: Rc<RefCell<Inner<T>>>,
}

struct Inner<T> {
    connected: bool,
    wait: Option<(Wait, T)>,
    set_event: Option<EventSlot>,
}

pub fn countdown<T>() -> (SetCountdown<T>, CountdownDone<T>) {
    trace!("countdown()");
    let set_event = Gui::with_mut(|gui| gui.alloc_event());
    let inner = Rc::new(RefCell::new(Inner {
        connected: true,
        wait: None,
        set_event: Some(set_event),
    }));
    let set = SetCountdown {
        inner: inner.clone(),
    };
    let done = CountdownDone {
        inner: inner,
    };
    (set, done)
}

impl<T> Drop for SetCountdown<T> {
    fn drop(&mut self) {
        trace!("SetCountdown::drop()");
        let mut inner = self.inner.borrow_mut();
        inner.connected = false;
    }
}

impl<T> Drop for CountdownDone<T> {
    fn drop(&mut self) {
        trace!("CountdownDone::drop()");
        let mut inner = self.inner.borrow_mut();
        inner.wait = None;
        if let Some(event) = inner.set_event.take() {
            Gui::with_mut(|gui| gui.clean_up_event(event));
        }
    }
}

impl<T> SetCountdown<T> {
    #[cfg(never)]
    pub fn set_or_extend(&self, dur: Duration, value: T) {
        let mut inner = self.inner.borrow_mut();

        let set_event = match inner.set_event {
            Some(e) => e,
            None => return,
        };

        let wait = Wait::new(dur);
        let value = match inner.wait.take() {
            Some((_, value)) => value,
            None => value,
        };
        inner.wait = Some((wait, value));

        // We have to wake the countdown's task so that it can re-block on the wait.
        Gui::fire_event(set_event);
    }

    #[cfg(never)]
    pub fn set_or_ignore(&self, dur: Duration, value: T) {
        let mut inner = self.inner.borrow_mut();

        let set_event = match inner.set_event {
            Some(e) => e,
            None => return,
        };

        if inner.wait.is_some() {
            return;
        }

        let wait = Wait::new(dur);
        inner.wait = Some((wait, value));

        // We have to wake the countdown's task so that it can re-block on the wait.
        Gui::fire_event(set_event);
    }

    #[cfg(never)]
    pub fn set_or_replace(&self, dur: Duration, value: T) {
        let mut inner = self.inner.borrow_mut();

        let set_event = match inner.set_event {
            Some(e) => e,
            None => return,
        };

        let wait = match inner.wait.take() {
            Some((wait, _)) => wait,
            None => Wait::new(dur),
        };
        inner.wait = Some((wait, value));

        // We have to wake the countdown's task so that it can re-block on the wait.
        Gui::fire_event(set_event);
    }

    pub fn set_or_reset(&self, dur: Duration, value: T) {
        trace!("SetCountdown::set_or_reset({:?}, _)", dur);
        let mut inner = self.inner.borrow_mut();

        let set_event = match inner.set_event {
            Some(e) => e,
            None => {
                trace!("- disconnected");
                return
            },
        };

        let wait = Wait::new(dur);
        inner.wait = Some((wait, value));

        // We have to wake the countdown's task so that it can re-block on the wait.
        trace!("- firing set_event");
        Gui::fire_event(set_event);
    }
}

impl<T> Stream for CountdownDone<T> {
    type Item = T;
    type Error = Error;

    fn poll(&mut self) -> Poll<Option<T>, Error> {
        trace!("CountdownDone::poll(_)");
        let mut inner = &mut*self.inner.borrow_mut();

        macro_rules! set_event {
            () => { inner.set_event.expect("set_event missing") };
        }

        match inner.wait.as_mut() {
            None => {
                trace!("- wait is None");
                if inner.connected {
                    trace!("- blocking on set_event");
                    let set_event = set_event!();
                    return Gui::with_mut(|gui| gui.block_on_stream_event(set_event));
                } else {
                    trace!("- disconnected");
                    return Ok(Async::Ready(None));
                }
            },
            Some(&mut (ref mut wait, _)) => {
                trace!("- wait is Some(_)");
                // Block on *either* wait or set_event completing.
                match wait.poll() {
                    Ok(Async::Ready(_)) => {
                        trace!("- wait ready");
                        // Fall-through.
                    },
                    Ok(Async::NotReady) => {
                        trace!("- wait not ready");
                        let set_event = set_event!();
                        return Gui::with_mut(|gui| gui.block_on_stream_event(set_event));
                    },
                    Err(err) => {
                        trace!("- wait failed: {}", err);
                        return Err(err.into());
                    },
                }
            },
        }

        trace!("- ready");
        let (_, value) = inner.wait.take().unwrap();
        Ok(Async::Ready(Some(value)))
    }
}
