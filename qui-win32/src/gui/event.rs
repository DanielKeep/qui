use std::cell::RefCell;
use std::rc::{self, Rc};
use futures::{Async, Future, Poll};
use ::{Error, other_error};
use ::gui::{EventSlot, Gui};

pub struct EventFuture(Rc<RefCell<EventFutureInner>>);
pub struct EventFutureSignal(rc::Weak<RefCell<EventFutureInner>>);

struct EventFutureInner {
    slot: EventSlot,
    connected: bool,
}

pub fn event_future() -> (EventFuture, EventFutureSignal) {
    let slot = Gui::with_mut(Gui::alloc_event);
    let inner = Rc::new(RefCell::new(EventFutureInner {
        slot: slot,
        connected: true,
    }));
    let signal = EventFutureSignal(Rc::downgrade(&inner));
    (EventFuture(inner), signal)
}

pub enum SignalResult {
    Signalled,
    Disconnected,
}

impl EventFutureSignal {
    pub fn signal(&self) -> SignalResult {
        if let Some(inner) = self.0.upgrade() {
            let inner = inner.borrow_mut();
            Gui::fire_event(inner.slot);
            SignalResult::Signalled
        } else {
            SignalResult::Disconnected
        }
    }
}

impl Drop for EventFutureSignal {
    fn drop(&mut self) {
        if let Some(inner) = self.0.upgrade() {
            let mut inner = inner.borrow_mut();
            inner.connected = false;
            Gui::fire_event(inner.slot);
        }
    }
}

impl Drop for EventFutureInner {
    fn drop(&mut self) {
        Gui::with_mut(|gui| gui.clean_up_event(self.slot));
    }
}

impl Future for EventFuture {
    type Item = ();
    type Error = Error;

    fn poll(&mut self) -> Poll<(), Error> {
        let inner = self.0.borrow();
        match Gui::with_mut(|gui| gui.block_on_event(inner.slot)) {
            Ok(Async::Ready(())) => Ok(Async::Ready(())),
            Ok(Async::NotReady) => {
                // We *might* be disconnected...
                if inner.connected {
                    Ok(Async::NotReady)
                } else {
                    other_error("event future disconected")
                }
            },
            Err(err) => Err(err),
        }
    }
}
