/*!
Keep-alive future implementation.
*/
use futures::{self, Async, Future, Poll};
use ::Error;
use super::{Event, EventSlot, Gui, SlabExt};

/**
Future that completes once all registered "keep-alive" objects are dead.

This is (loosely) equivalent to "the GUI has naturally shut down".
*/
pub struct AllKeepAliveDead {
    event: Option<EventSlot>,
}

impl AllKeepAliveDead {
    pub fn new() -> Self {
        trace!("AllKeepAliveDead::new()");
        Gui::with_mut(|gui| {
            // Ensure we have enough space in the many varied things into which we place other things.
            gui.keep_alive_events.reserve(1);
            let e_entry = gui.events.some_vacant_entry();

            // Add the event.
            let event_slot = e_entry.index();
            gui.keep_alive_events.push(event_slot);
            e_entry.insert(Event::Pending);

            AllKeepAliveDead {
                event: Some(event_slot),
            }
        })
    }
}

impl Drop for AllKeepAliveDead {
    fn drop(&mut self) {
        trace!("AllKeepAliveDead::drop()");
        // Take care of the event.
        if let Some(event) = self.event {
            Gui::with_mut(|gui| gui.clean_up_event(event));
        }
    }
}

impl Future for AllKeepAliveDead {
    type Item = ();
    type Error = Error;

    fn poll(&mut self) -> Poll<(), Error> {
        trace!("AllKeepAliveDead::poll()");
        Gui::with_mut(|gui| {
            let event = self.event.expect("AllKeepAliveDead.empty is empty");
            let event = &mut gui.events[event];
            match *event {
                Event::Pending | Event::Blocking(_) => {
                    *event = Event::Blocking(futures::task::park());
                    Ok(Async::NotReady)
                },
                Event::Fired => Ok(Async::Ready(())),
                Event::Zombie => panic!("tried to poll a zombie event"),
            }
        })
    }
}
