pub use self::countdown::{countdown, CountdownDone, SetCountdown};
pub use self::event::{event_future, EventFuture, EventFutureSignal};
pub use self::keep_alive::AllKeepAliveDead;
pub use self::wait::Wait;

use std::cell::RefCell;
use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::marker::PhantomData;
use std::mem;
use std::sync::Arc;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering::SeqCst;
use std::time::{Duration, Instant};
use futures::{self, Async, Future};
use futures::task::Unpark;
use itertools::Itertools;
use slab::Slab;
use winapi;
use wui;
use ::{Error, Result};
use ::util::Alive;

mod countdown;
mod event;
mod keep_alive;
mod wait;

thread_local! {
    /**
    This contains the global `Gui` state value.

    This should only ever *actually* be non-`None` in the main UI thread.
    */
    static GUI: RefCell<Option<Gui>> = RefCell::new(None)
}

/**
Represents an index into the internal task slab.
*/
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
struct TaskSlot(usize);

impl From<usize> for TaskSlot {
    fn from(v: usize) -> TaskSlot {
        TaskSlot(v)
    }
}

impl From<TaskSlot> for usize {
    fn from(v: TaskSlot) -> usize {
        v.0
    }
}

/**
Represents an index into the internal event slab.
*/
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct EventSlot(usize);

impl From<usize> for EventSlot {
    fn from(v: usize) -> EventSlot {
        EventSlot(v)
    }
}

impl From<EventSlot> for usize {
    fn from(v: EventSlot) -> usize {
        v.0
    }
}

/**
The central value that "owns" the UI and event loop.

It can *only* be accessed from the UI thread.
*/
pub struct Gui {
    /**
    A list of futures that need to be spawned.
    */
    tasks_to_spawn: Vec<Box<Future<Item=(), Error=Error>>>,

    /**
    A list of tasks that need to be "unparked".
    */
    tasks_ready_to_unpark: Vec<TaskSlot>,

    /**
    Should the main task be unparked?
    */
    main_task_ready_to_unpark: bool,

    /**
    This is a collection of weak pointers that will implicitly keep the GUI "alive" while they are valid.  This should be used for top-level windows; as long as there is *at least* one top-level window, the GUI will continue to run.

    This does *not* prevent the GUI from being *explicitly* exited.  This exists to ensure that once all top-level windows are destroyed, the GUI closes automatically.
    */
    keep_alive: Vec<Box<Alive>>,

    /**
    Table records all events that need to be fired when all keep-alives are dead.
    */
    keep_alive_events: Vec<EventSlot>,

    /**
    Contains all pending delays.

    Delay expiries are stored as instants in time to avoid having to go through and mutate them all every tick.
    */
    delay_wake_instants: BinaryHeap<WakeInstant>,

    /**
    This stores all the registered events.
    */
    events: Slab<Event, EventSlot>,

    /**
    The native "wake up" event.
    */
    native_wake_up_event: wui::Event,

    /**
    Prevent this type from being moved to, or accessed from, another thread.

    Specifically, we want `!Send + !Sync`.
    */
    _marker: PhantomData<*const ()>,
}

impl Gui {
    fn new() -> Result<Gui> {
        let wake_up = wui::EventBuilder::new()
            .desired_access({
                use wui::sync_rights::*;
                Delete | Synchronize | EventModifyState
            })
            .create()?;

        Ok(Gui {
            tasks_to_spawn: vec![],
            tasks_ready_to_unpark: vec![],
            main_task_ready_to_unpark: false,
            keep_alive: Vec::with_capacity(64),
            keep_alive_events: vec![],
            delay_wake_instants: BinaryHeap::with_capacity(64),
            events: Slab::with_capacity(1024),
            native_wake_up_event: wake_up,
            _marker: PhantomData,
        })
    }

    pub fn init<F, R>(f: F) -> Result<R>
    where F: FnOnce() -> R {
        init_gui(f)
    }

    pub fn run<F>(future: F) -> GuiResult<F::Item>
    where F: Future<Error=Error> {
        run_main(future)
    }

    /**
    Fires the specified event.

    **Important**: this *cannot* be called whilst you have the `Gui` borrowed (via either `Gui::with` or `Gui::with_mut`).
    */
    pub fn fire_event(event: EventSlot) {
        let mut unpark = None;
        Gui::with_mut(|gui| {
            fire_event(event, &mut gui.events, |task| unpark = Some(task.clone()));
        });
        if let Some(task) = unpark {
            task.unpark();
        }
    }

    pub fn with<F, R>(f: F) -> R where F: FnOnce(&Self) -> R {
        GUI.with(|gui| {
            let gui = gui.borrow();
            assert!(gui.is_some());
            f(gui.as_ref().unwrap())
        })
    }

    pub fn with_mut<F, R>(f: F) -> R where F: FnOnce(&mut Self) -> R {
        GUI.with(|gui| {
            let mut gui = gui.borrow_mut();
            assert!(gui.is_some());
            f(gui.as_mut().unwrap())
        })
    }

    /**
    Allocates a general event slot.

    This can be used with `Gui::fire_event` to wake a blocked task.
    */
    pub fn alloc_event(&mut self) -> EventSlot {
        let entry = self.events.some_vacant_entry();
        let event_slot = entry.index();
        entry.insert(Event::Pending);
        event_slot
    }

    /**
    Blocks the current task on the given event.
    */
    pub fn block_on_event(&mut self, event: EventSlot) -> Result<Async<()>> {
        let e = &mut self.events[event];
        match *e {
            // Pending or blocking, we'll set blocking afterwards.
            Event::Pending | Event::Blocking(_) => (),

            // Already fired.  This *shouldn't* happen, but eh, whatever.
            Event::Fired => {
                return Ok(Async::Ready(()));
            },

            // Wait... *what?!*
            Event::Zombie => panic!("tried to poll a zombie event"),
        }

        // Update the blocking state.
        *e = Event::Blocking(futures::task::park());

        Ok(Async::NotReady)
    }

    /**
    Blocks the current task on the given stream event.

    This *always* returns `Ok(Async::NotReady)`.
    */
    pub fn block_on_stream_event<T>(&mut self, event: EventSlot) -> Result<Async<T>> {
        let e = &mut self.events[event];
        match *e {
            Event::Pending | Event::Blocking(_) | Event::Fired => (),

            // Wait... *what?!*
            Event::Zombie => panic!("tried to poll a zombie event {:?}", event),
        }

        // Update the blocking state.
        *e = Event::Blocking(futures::task::park());

        Ok(Async::NotReady)
    }

    /**
    Processes the specified event in the event the corresponding `Future` has been dropped.

    Should **only** be called from a `Future`'s `Drop` implementation.
    */
    pub fn clean_up_event(&mut self, event: EventSlot) {
        match self.events[event] {
            Event::Zombie => {
                panic!("cannot clean up zombified event {:?}", event);
            },
            Event::Pending | Event::Blocking(_) => {
                self.events[event] = Event::Zombie;
            },
            Event::Fired => {
                // We can deallocate the slot.
                self.events.remove(event);
            },
        }
    }

    /**
    Processes the specified event in the event the corresponding `Stream` has been dropped.

    Should **only** be called from a `Stream`'s `Drop` implementation.
    */
    pub fn clean_up_stream_event(&mut self, event: EventSlot) {
        match self.events[event] {
            Event::Zombie => {
                panic!("cannot clean up zombified event {:?}", event);
            },
            Event::Pending | Event::Blocking(_) | Event::Fired => {
                // We can deallocate the slot.
                self.events.remove(event);
            },
        }
    }

    pub fn register_keep_alive<T: 'static + Alive>(&mut self, keep_alive: T) {
        self.keep_alive.push(Box::new(keep_alive));
    }

    /**
    Indicate to the event loop that it should wake up.
    */
    pub fn schedule_wake_up(&self) -> Result<()> {
        self.native_wake_up_event.set()
    }

    pub fn spawn_task<F>(&mut self, future: F)
    where F: 'static + Future<Item=(), Error=Error> {
        self.tasks_to_spawn.push(Box::new(future));
    }
}

/**
Performs necessary set-up and tear-down of a `Gui`.
*/
fn init_gui<F, R>(f: F) -> Result<R>
where F: FnOnce() -> R {
    GUI.with(|gui| -> Result<()> {
        let mut gui = gui.borrow_mut();
        assert!(gui.is_none(), "`Gui` has already been started");
        *gui = Some(Gui::new()?);
        wui::co_initialize_ex(None)?;
        Ok(())
    })?;

    let r = f();

    wui::co_uninitialize();
    GUI.with(|gui| {
        let mut gui = gui.borrow_mut();
        assert!(gui.is_some(), "`Gui` is not running; cannot shut down");
        let _ = gui.take();
    });

    Ok(r)
}

/**
Information needed for a running task.
*/
struct GuiTask {
    /// The task itself.
    task: futures::task::Spawn<Box<Future<Item=(), Error=Error>>>,

    /// Strong reference to the corresponding task handle.
    handle: Arc<TaskHandle>,
}

/**
This type is used to "unpark" the main task.
*/
struct MainTaskHandle;

impl Unpark for MainTaskHandle {
    fn unpark(&self) {
        trace!("MainTaskHandle::unpark()");
        Gui::with_mut(|gui| {
            gui.main_task_ready_to_unpark = true;
            gui.schedule_wake_up().expect("MainTaskHandle: could not schedule wake-up");
        })
    }
}

/**
This type is used to "unpark" tasks.

Futures need to hold on to an `Arc<Unpark>` implemented by this type in order to handle unparking.  The problem is that these futures can outlive the task they parked.

As a result, these handles exist so long as the task or at least one future that's parked it exist.  When the task ends, it needs to reach into the handle and clear the `TaskSlot` stored within.  This is important because that `TaskSlot` may be re-used.
*/
struct TaskHandle {
    /**
    The `TaskSlot` this `TaskHandle` maps to.

    Is only `None` when the task no longer exists.
    */
    task: AtomicUsize,
}

impl TaskHandle {
    fn new(slot: TaskSlot) -> TaskHandle {
        if usize::from(slot) == !0 {
            panic!("can't create a `TaskHandle` for `{:?}`", slot);
        }

        TaskHandle {
            task: AtomicUsize::new(slot.into()),
        }
    }

    fn get_task(&self) -> Option<TaskSlot> {
        let v = self.task.load(SeqCst);
        if v == !0 {
            None
        } else {
            Some(TaskSlot::from(v))
        }
    }

    fn set_task(&self, v: Option<TaskSlot>) {
        let v = match v {
            Some(v) => {
                if usize::from(v) == !0 {
                    panic!("can't store `{:?}` in `TaskHandle`", v);
                }
                v.into()
            },
            None => !0,
        };
        self.task.store(v, SeqCst);
    }
}

impl Eq for TaskHandle {}

impl Ord for TaskHandle {
    fn cmp(&self, other: &Self) -> Ordering {
        self.task.load(SeqCst).cmp(&other.task.load(SeqCst))
    }
}

impl PartialEq for TaskHandle {
    fn eq(&self, other: &Self) -> bool {
        self.task.load(SeqCst).eq(&other.task.load(SeqCst))
    }
}

impl PartialOrd for TaskHandle {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.task.load(SeqCst).partial_cmp(&other.task.load(SeqCst))
    }
}

impl Unpark for TaskHandle {
    fn unpark(&self) {
        trace!("TaskHandle::unpark()");
        if let Some(task) = self.get_task() {
            trace!("- task: {:?}", task);
            Gui::with_mut(|gui| {
                gui.tasks_ready_to_unpark.push(task);
                gui.schedule_wake_up().expect("TaskHandle: could not schedule wake-up");
            })
        }
    }
}

/**
This represents a moment in time after which a wake event should be fired.

The comparison traits are implemented such that this is suitable for use in a max heap to sort the "earliest" instant first.
*/
#[derive(Clone, Eq, PartialEq, Ord, Debug)]
pub struct WakeInstant {
    instant: Instant,
    event: EventSlot,
}

impl PartialOrd for WakeInstant {
    fn partial_cmp(&self, other: &WakeInstant) -> Option<Ordering> {
        Some(match self.instant.cmp(&other.instant) {
            Ordering::Equal => self.event.cmp(&other.event),
            other => other.reverse()
        })
    }
}

pub enum Event {
    /**
    The event hasn't happened yet, and nothing is blocked on it.
    */
    Pending,
    /**
    The event hasn't happened yet, but a future is blocked on it.
    */
    Blocking(futures::task::Task),
    /**
    The event has already happened.
    */
    Fired,
    /**
    Whether or not the event has happened, the future that created it is gone.
    */
    Zombie,
}

boolish! {
    #[derive(Copy, Clone, PartialEq, Eq)]
    boolish ExitEventLoop {
        Yes = true,
        No = false,
    }
}

#[derive(Debug)]
pub enum GuiResult<T> {
    Ok(T),
    Exit(i32),
    Err(Error),
}

fn run_main<F>(main: F) -> GuiResult<F::Item>
where F: Future<Error=Error> {
    trace!("run_main(_)");
    let mut main_result = None;
    let mut main_task = futures::task::spawn(main);
    let main_handle = Arc::new(MainTaskHandle);

    match run(&mut || {
        trace!("run_main: polling main future");
        match main_task.poll_future(main_handle.clone()) {
            Ok(Async::Ready(value)) => {
                main_result = Some(Ok(value));
                ExitEventLoop::Yes
            },
            Ok(Async::NotReady) => {
                ExitEventLoop::No
            },
            Err(err) => {
                main_result = Some(Err(err));
                ExitEventLoop::Yes
            },
        }
    }) {
        Ok(Some(code)) => GuiResult::Exit(code),
        Ok(None) => match main_result {
            Some(Ok(v)) => GuiResult::Ok(v),
            Some(Err(err)) => GuiResult::Err(err),
            None => {
                // Uhhhh... wait, what?
                panic!("event loop exited with no result");
            },
        },
        Err(err) => GuiResult::Err(err),
    }
}

fn run(poll_main: &mut FnMut() -> ExitEventLoop) -> Result<Option<i32>> {
    use wui::MsgWaitResult;

    trace!("run: starting main loop");

    // Forcibly poll the main task to get things started.
    if let ExitEventLoop::Yes = poll_main() {
        return Ok(None);
    }

    let mut tasks: Slab<GuiTask, TaskSlot> = Slab::with_capacity(64);
    let mut wait_on = vec![];
    let mut task_cache = vec![];
    let mut task_poll_cache = vec![];

    const WAKE_UP_HANDLE: usize = 0;

    Gui::with(|gui| {
        use wui::AsRaw;
        assert_eq!(wait_on.len(), WAKE_UP_HANDLE);
        wait_on.push(gui.native_wake_up_event.as_raw())
    });

    /*
    Do a keep-alive check *now*.  The reason for *this* is that the user *might* have forgotten to actually create/register anything.  In that case, we could end up blocking forever, waiting for nothing to happen.

    If there's nothing to keep the GUI alive, this will signal a wake-up.
    */
    process_keep_alives(&mut task_cache);

    loop {
        trace!("run: tick at {:?}", Instant::now());

        /*
        Spawn new tasks.
        */
        Gui::with_mut(|gui| -> Result<()> {
            if gui.tasks_to_spawn.len() == 0 {
                return Ok(());
            }

            trace!("run: spawning {:?} new tasks", gui.tasks_to_spawn.len());
            tasks.reserve_exact(gui.tasks_to_spawn.len());

            for future in gui.tasks_to_spawn.drain(..) {
                // Give it a place to run.
                let entry = tasks.some_vacant_entry();
                let slot = entry.index();
                let handle = Arc::new(TaskHandle::new(slot));
                entry.insert(GuiTask {
                    task: futures::task::spawn(future),
                    handle: handle,
                });

                // Schedule it to be polled.
                gui.tasks_ready_to_unpark.push(slot);
            }

            gui.schedule_wake_up()?;
            Ok(())
        })?;

        /*
        Why not handle timeouts when we're woken up due to a timeout?

        Well, before we resume waiting, we need to recompute how long until the next timeout.  But if we're going to go through locking the `Gui` and computing that... well, we might as well just handle any timeouts that have expired.

        One point of design is whether to do this in a loop or not.  We *don't* in this case, because we don't want someone using a 0-duration wait as a "yield" to block the process.
        */
        let now = Instant::now();
        let timeout = process_timeouts(now, &mut task_cache)?;

        trace!("run: sleeping (timeout: {:?})", timeout);

        let wake_reason = wui::msg_wait_for_multiple_objects_ex(
            wait_on.iter().cloned(), timeout,
            winapi::QS_ALLINPUT, winapi::MWMO_ALERTABLE)?;

        trace!("run: woke up because {:?}", wake_reason);

        match wake_reason {
            MsgWaitResult::HandleReady(handle) => {
                match handle {
                    WAKE_UP_HANDLE => {
                        // Really, waking up was enough.  Toilet, a quick snack, then back to bed.
                    },
                    _ => {
                        panic!("woke up due to unknown handle");
                    }
                }
            },

            MsgWaitResult::Message => {
                match process_messages()? {
                    Some(code) => return Ok(Some(code)),
                    None => (),
                }
            },

            MsgWaitResult::MutexAbandoned(_) => unimplemented!(),

            MsgWaitResult::Apc => {/* do nothing */},

            MsgWaitResult::Timeout => {/* handled above */},

            MsgWaitResult::Unknown => {/* ignore */},
        }

        /*
        Check the keep-alives now.

        If a keep-alive is going to die as a result of state changes prompted by this loop, it should have done so by now.
        */
        process_keep_alives(&mut task_cache);

        /*
        Re-poll the main task, if it needs it.
        */
        let ready_to_poll_main = Gui::with_mut(|gui| {
            mem::replace(&mut gui.main_task_ready_to_unpark, false)
        });
        if ready_to_poll_main {
            if poll_main() == ExitEventLoop::Yes {
                return Ok(None);
            }
        }

        /*
        Re-poll other tasks that are ready.
        */
        trace!("run: polling non-main tasks");
        Gui::with_mut(|gui| {
            task_poll_cache.clear();
            task_poll_cache.extend(gui.tasks_ready_to_unpark.drain(..));
            task_poll_cache.sort();
        });

        trace!("- got {} tasks to poll", task_poll_cache.len());
        let poll_tasks = task_poll_cache.drain(..)
            .unique();

        for task in poll_tasks {
            let remove_task = {
                let task = &mut tasks[task];
                match task.task.poll_future(task.handle.clone()) {
                    Ok(Async::Ready(())) => {
                        trace!("- task ready");
                        task.handle.set_task(None);
                        true
                    },
                    Ok(Async::NotReady) => {
                        // Has *presumably* been parked somewhere.  Nothing more to do.
                        trace!("- task not ready");
                        false
                    },
                    Err(err) => {
                        // TODO: something less destructive.
                        trace!("- task failed: {}", err);
                        return Err(err);
                    },
                }
            };
            if remove_task {
                tasks.remove(task);
            }
        }
    }
}

fn process_keep_alives(task_cache: &mut Vec<futures::task::Task>) {
    trace!("process_keep_alives(_)");

    task_cache.clear();
    let to_unpark = task_cache;

    Gui::with_mut(|gui| {
        // This was a triumph.
        let handles = &mut gui.keep_alive;

        // I'm making a note here...
        if handles.len() != 0 {
            // ...huge succe--*BANG* *thump*
            let mut next = handles.len() - 1;
            loop {
                // Check the liveness of all handles, and remove ones that are dead.  We don't care about ordering, so swap_remove is fine.
                if !handles[next].is_alive() {
                    handles.swap_remove(next);
                }

                if next == 0 {
                    break;
                } else {
                    next -= 1;
                }
            }
        }

        // Pull out the list of tasks to notify, if everything's dead.
        if handles.len() == 0 {
            trace!("- no keep-alives left ({} events)", gui.keep_alive_events.len());
            for event in gui.keep_alive_events.drain(..) {
                fire_event(event, &mut gui.events, |task| to_unpark.push(task.clone()));
            }
        }
    });

    // Unpark the tasks.
    if to_unpark.len() > 0 {
        trace!("- unparking {} tasks", to_unpark.len());
    }
    for task in to_unpark.drain(..) {
        task.unpark();
    }
}

fn process_timeouts(
    now: Instant,
    task_cache: &mut Vec<futures::task::Task>,
) -> Result<Option<Duration>> {
    trace!("process_timeouts({:?}, _)", now);

    let dur_zero = Duration::new(0, 0);

    task_cache.clear();
    let to_unpark = task_cache;

    let dur_to_next = Gui::with_mut(|gui| {
        let mut dwis = mem::replace(&mut gui.delay_wake_instants, BinaryHeap::new());

        loop {
            match dwis.peek() {
                Some(wi) => {
                    let until = dur_from_until(now, wi.instant);
                    if until == dur_zero {
                        fire_event(wi.event, &mut gui.events, |task| to_unpark.push(task.clone()));
                    }
                    else {
                        // We're done.
                        break;
                    }
                },
                None => {
                    // No more wake instants; time to call it a day.
                    break;
                },
            };

            let _ = dwis.pop();
        }

        // Replace `dwis`, and check that nothing got added somehow.
        assert_eq!(gui.delay_wake_instants.len(), 0);
        let dur_to_next = dwis.peek().map(|wi| dur_from_until(now, wi.instant));
        mem::replace(&mut gui.delay_wake_instants, dwis);

        dur_to_next
    });

    /*
    Unpark the tasks now.  We have to do this outside `Gui::with_mut` because the task handles will need to mutate the `Gui` object.
    */
    if to_unpark.len() > 0 {
        trace!("- unparking {} tasks", to_unpark.len());
    }
    for task in to_unpark.drain(..) {
        task.unpark();
    }

    // All done.
    Ok(dur_to_next)
}

fn fire_event<F>(event: EventSlot, events: &mut Slab<Event, EventSlot>, on_unpark: F)
where F: FnOnce(&futures::task::Task) {
    let (set_fired, remove) = match events[event] {
        Event::Pending => (true, false),
        Event::Zombie => (false, true),
        Event::Fired => (false, false) /* Do nothing. */,
        Event::Blocking(ref task) => {
            on_unpark(task);

            /*
            *Don't* remove.  The reason is that although the future *shouldn't* try to re-use the event slot... we can't actually *guarantee* that won't happen.  It's a little bit safer to just wait until the future goes away.

            If we screw anything up, we'll be in a situation where a future is pointing to an event slot it doesn't own, and we can't currently detect that.
            */
            (true, false)
        },
    };

    if set_fired {
        events[event] = Event::Fired;
    }

    if remove {
        events.remove(event);
    }
}

fn dur_from_until(from: Instant, until: Instant) -> Duration {
    if until <= from {
        Duration::new(0, 0)
    } else {
        until.duration_since(from)
    }
}

/**
Processes all pending window messages.

# Result

Returns `Some(code)` if a `WM_QUIT` message was received, where `code` is intended as the process' exit code.  Returns `None` in all other cases.
*/
fn process_messages() -> Result<Option<i32>> {
    use winapi::{MSG, WM_QUIT};
    use wui::MsgExt;

    trace!("process_messages()");

    while let Some(msg) = MSG::peek(None, None, wui::remove_msg::Remove)? {
        match msg {
            MSG { message: WM_QUIT, wParam: code, .. } => {
                trace!("process_messages: got WM_QUIT; exiting");
                return Ok(Some(code as i32));
            },

            msg => {
                // TODO: handle dialog window processing.
                msg.translate();
                msg.dispatch();
            }
        }
    }

    Ok(None)
}

trait SlabExt<T, I> {
    fn some_vacant_entry(&mut self) -> ::slab::VacantEntry<T, I>;
}

impl<T, I: Into<usize> + From<usize>> SlabExt<T, I> for ::slab::Slab<T, I> {
    fn some_vacant_entry(&mut self) -> ::slab::VacantEntry<T, I> {
        if self.vacant_entry().is_none() {
            // Double capacity (this is *extra* space).
            let len = self.len();
            self.reserve_exact(len);
        }
        self.vacant_entry().unwrap()
    }
}
