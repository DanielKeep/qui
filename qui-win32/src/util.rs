// use std::cell::Cell;
// use std::ops::Deref;
use std::rc::Rc;
use std::rc::Weak as RcWeak;
// use std::sync::Weak as ArcWeak;

pub trait Alive {
    fn is_alive(&self) -> bool;
}

// impl<T> Alive for ArcWeak<T> {
//     fn is_alive(&self) -> bool {
//         self.upgrade().is_some()
//     }
// }

impl<T> Alive for RcWeak<T> {
    fn is_alive(&self) -> bool {
        self.upgrade().is_some()
    }
}

#[derive(Debug)]
pub enum FlexRc<T: ?Sized> {
    Strong(Rc<T>),
    Weak(RcWeak<T>),
}

impl<T: ?Sized> FlexRc<T> {
    pub fn downgraded(&self) -> RcWeak<T> {
        use self::FlexRc::*;
        match *self {
            Strong(ref rc) => Rc::downgrade(rc),
            Weak(ref weak) => (*weak).clone(),
        }
    }

    pub fn upgraded(&self) -> Option<Rc<T>> {
        use self::FlexRc::*;
        match *self {
            Strong(ref rc) => Some(rc.clone()),
            Weak(ref weak) => weak.upgrade(),
        }
    }

    pub fn downgraded_flex(&self) -> FlexRc<T> {
        match *self {
            FlexRc::Strong(ref rc) => Rc::downgrade(rc).into(),
            FlexRc::Weak(_) => (*self).clone(),
        }
    }

    pub fn upgraded_flex(&self) -> (FlexRc<T>, bool) {
        match *self {
            FlexRc::Strong(_) => ((*self).clone(), true),
            FlexRc::Weak(ref weak) => match weak.upgrade() {
                Some(rc) => (rc.into(), true),
                None => (weak.clone().into(), false),
            },
        }
    }
}

impl<T: ?Sized> Clone for FlexRc<T> {
    fn clone(&self) -> Self {
        match *self {
            FlexRc::Strong(ref rc) => FlexRc::Strong(rc.clone()),
            FlexRc::Weak(ref wk) => FlexRc::Weak(wk.clone()),
        }
    }
}

impl<T: ?Sized> From<Rc<T>> for FlexRc<T> {
    fn from(v: Rc<T>) -> Self {
        FlexRc::Strong(v)
    }
}

impl<T: ?Sized> From<RcWeak<T>> for FlexRc<T> {
    fn from(v: RcWeak<T>) -> Self {
        FlexRc::Weak(v)
    }
}

// #[derive(Debug)]
// pub struct Pinnable<T> {
//     pins: Cell<usize>,
//     value: T,
// }

// impl<T> Pinnable<T> {
//     pub fn new(value: T) -> Self {
//         Pinnable {
//             pins: Cell::new(0),
//             value: value,
//         }
//     }

//     pub fn is_pinned(&self) -> bool {
//         self.pins.get() != 0
//     }

//     pub fn pin(&self) -> Pinned<T> {
//         let inc_pins = match self.pins.get().checked_add(1) {
//             Some(v) => v,
//             None => panic!("pin count overflow"),
//         };
//         self.pins.set(inc_pins);
//         Pinned(self)
//     }
// }

// pub struct Pinned<'a, T: 'a>(&'a Pinnable<T>);

// impl<'a, T: 'a> Deref for Pinned<'a, T> {
//     type Target = T;
//     fn deref(&self) -> &T {
//         &self.0.value
//     }
// }

// impl<'a, T: 'a> Drop for Pinned<'a, T> {
//     fn drop(&mut self) {
//         self.0.pins.set(self.0.pins.get() - 1);
//     }
// }
