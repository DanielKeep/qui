#![feature(question_mark)]
#[macro_use] extern crate futures;
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate log;
#[macro_use] extern crate scoped_tls;
#[macro_use] extern crate wui;
extern crate itertools;
extern crate slab;
extern crate winapi;

#[doc(hidden)]
#[macro_use] pub mod internal;

pub mod error;
pub mod event;
pub mod ext;
pub mod gui;
pub mod hello;
pub mod util;
pub mod window;

pub use error::{Error, Result};

fn disconnect_error<T>() -> ::Result<T> {
    other_error("future source disconnected")
}

fn other_error<T, S>(msg: S) -> ::Result<T> where S: Into<String> {
    Err(From::from(std::io::Error::new(std::io::ErrorKind::Other, msg.into())))
}
