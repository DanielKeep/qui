use std::collections::VecDeque;
use std::cell::RefCell;
use std::rc::{self, Rc};
use futures::{Async, Future, Poll};
use futures::stream::Stream;
use ::{disconnect_error, Error, Result};
use ::gui::{Gui, EventSlot};
use ::window::MessageResult;

pub type Callback<Arg, Ret> = Option<Box<CallbackFn<Arg, Ret>>>;
pub type CallbackFn<Arg, Ret> = FnMut(Arg) -> Result<MessageResult<Ret>>;

pub struct EventHandlers<Arg, Ret>
where Arg: Clone {
    callback: EventCallback<Arg, Ret>,
    streams: EventStreams<Arg>,
}

pub struct EventCallback<Arg, Ret> {
    callback: RefCell<Callback<Arg, Ret>>,
}

pub struct EventStreams<Arg>
where Arg: Clone {
    streams: RefCell<Vec<rc::Weak<RefCell<EventStreamInner<Arg>>>>>,
}

pub struct EventStream<Arg>
where Arg: Clone {
    inner: Rc<RefCell<EventStreamInner<Arg>>>,
}

struct EventStreamInner<Arg>
where Arg: Clone {
    payloads: VecDeque<Arg>,
    poll_event: EventSlot,
    connected: bool,
}

pub struct EventFutures<Arg>
where Arg: Clone {
    futures: RefCell<Vec<rc::Weak<RefCell<EventFutureInner<Arg>>>>>,
}

pub struct EventFuture<Arg>
where Arg: Clone {
    inner: Rc<RefCell<EventFutureInner<Arg>>>,
}

struct EventFutureInner<Arg>
where Arg: Clone {
    payload: Option<Arg>,
    poll_event: EventSlot,
    connected: bool,
}

impl<Arg, Ret> EventHandlers<Arg, Ret>
where Arg: Clone {
    pub fn new() -> Self {
        EventHandlers {
            callback: EventCallback::new(),
            streams: EventStreams::new(),
        }
    }

    pub fn callback<F>(&self, callback: F)
    where F: 'static + FnMut(Arg) -> Result<MessageResult<Ret>> {
        self.callback.callback(callback);
    }

    pub fn invoke(&self, arg: Arg) -> Result<MessageResult<Ret>> {
        let r = self.callback.invoke(arg.clone())?;
        self.streams.invoke(arg);
        Ok(r)
    }

    pub fn stream(&self) -> EventStream<Arg> {
        self.streams.stream()
    }
}

impl<Arg, Ret> EventCallback<Arg, Ret> {
    pub fn new() -> Self {
        EventCallback {
            callback: RefCell::new(None),
        }
    }

    pub fn callback<F>(&self, callback: F)
    where F: 'static + FnMut(Arg) -> Result<MessageResult<Ret>> {
        *self.callback.borrow_mut() = Some(Box::new(callback));
    }

    pub fn invoke(&self, arg: Arg) -> Result<MessageResult<Ret>> {
        if let Some(callback) = self.callback.borrow_mut().as_mut() {
            (callback)(arg)
        } else {
            Ok(MessageResult::CallDefault)
        }
    }
}

impl<Arg> EventStreams<Arg>
where Arg: Clone {
    pub fn new() -> Self {
        EventStreams {
            streams: RefCell::new(vec![]),
        }
    }

    pub fn invoke(&self, arg: Arg) {
        let mut streams = self.streams.borrow_mut();
        streams.retain(|inner| {
            if let Some(inner) = inner.upgrade() {
                let mut inner = inner.borrow_mut();
                inner.payloads.push_back(arg.clone());
                Gui::fire_event(inner.poll_event);
                true
            } else {
                false
            }
        });
    }

    pub fn stream(&self) -> EventStream<Arg> {
        let mut streams = self.streams.borrow_mut();
        let (stream, inner) = EventStream::new();
        streams.push(Rc::downgrade(&inner));
        stream
    }
}

impl<Arg> Drop for EventStreams<Arg>
where Arg: Clone {
    fn drop(&mut self) {
        // Disconnect all streams, and wake them up so that they can shut down.
        for inner in &*self.streams.borrow_mut() {
            if let Some(inner) = inner.upgrade() {
                let mut inner = inner.borrow_mut();
                inner.connected = false;
                Gui::fire_event(inner.poll_event);
            }
        }
    }
}

impl<Arg> EventStream<Arg>
where Arg: Clone {
    fn new() -> (EventStream<Arg>, Rc<RefCell<EventStreamInner<Arg>>>) {
        Gui::with_mut(|gui| {
            // Add the event.
            // TODO: switch to a special `StreamEvent` enum?  We don't need all the variants.
            let event_slot = gui.alloc_event();

            let inner = EventStreamInner {
                payloads: VecDeque::new(),
                poll_event: event_slot,
                connected: true,
            };

            let inner = Rc::new(RefCell::new(inner));
            let stream = EventStream {
                inner: inner.clone(),
            };

            (stream, inner)
        })
    }
}

impl<Arg> Drop for EventStreamInner<Arg>
where Arg: Clone {
    fn drop(&mut self) {
        Gui::with_mut(|gui| gui.clean_up_stream_event(self.poll_event));
    }
}

impl<Arg> Stream for EventStream<Arg>
where Arg: Clone {
    type Item = Arg;
    type Error = Error;

    fn poll(&mut self) -> Poll<Option<Arg>, Error> {
        let mut inner = self.inner.borrow_mut();

        if let Some(arg) = inner.payloads.pop_front() {
            return Ok(Async::Ready(Some(arg)));
        }

        if !inner.connected {
            return Ok(Async::Ready(None));
        }

        Gui::with_mut(|gui| gui.block_on_stream_event(inner.poll_event))
    }
}

impl<Arg> EventFutures<Arg>
where Arg: Clone {
    pub fn new() -> Self {
        EventFutures {
            futures: RefCell::new(vec![]),
        }
    }

    pub fn invoke(&self, arg: Arg) {
        let mut futures = self.futures.borrow_mut();
        for inner in futures.drain(..).flat_map(|i| i.upgrade()) {
            let mut inner = inner.borrow_mut();
            inner.payload = Some(arg.clone());
            Gui::fire_event(inner.poll_event);
        }
    }

    pub fn future(&self) -> EventFuture<Arg> {
        let mut futures = self.futures.borrow_mut();
        let (future, inner) = EventFuture::new();
        futures.push(Rc::downgrade(&inner));
        future
    }
}

impl<Arg> Drop for EventFutures<Arg>
where Arg: Clone {
    fn drop(&mut self) {
        for inner in &*self.futures.borrow_mut() {
            if let Some(inner) = inner.upgrade() {
                let mut inner = inner.borrow_mut();
                inner.connected = false;
                Gui::fire_event(inner.poll_event);
            }
        }
    }
}

impl<Arg> EventFuture<Arg>
where Arg: Clone {
    fn new() -> (EventFuture<Arg>, Rc<RefCell<EventFutureInner<Arg>>>) {
        let event_slot = Gui::with_mut(Gui::alloc_event);

        let inner = EventFutureInner {
            payload: None,
            poll_event: event_slot,
            connected: true,
        };

        let inner = Rc::new(RefCell::new(inner));
        let future = EventFuture {
            inner: inner.clone(),
        };

        (future, inner)
    }
}

impl<Arg> Drop for EventFutureInner<Arg>
where Arg: Clone {
    fn drop(&mut self) {
        Gui::with_mut(|gui| gui.clean_up_event(self.poll_event));
    }
}

impl<Arg> Future for EventFuture<Arg>
where Arg: Clone {
    type Item = Arg;
    type Error = Error;

    fn poll(&mut self) -> Poll<Arg, Error> {
        let mut inner = self.inner.borrow_mut();

        match Gui::with_mut(|gui| gui.block_on_event(inner.poll_event)) {
            Ok(Async::Ready(())) => {
                if let Some(arg) = inner.payload.take() {
                    Ok(Async::Ready(arg))
                } else {
                    panic!("EventFuture signalled without payload");
                }
            },
            Ok(Async::NotReady) => {
                if !inner.connected {
                    disconnect_error()
                } else {
                    Ok(Async::NotReady)
                }
            },
            Err(err) => Err(err),
        }
    }
}
