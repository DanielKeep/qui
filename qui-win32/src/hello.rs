use std::rc::Rc;
use gui::Gui;
use wui::{self, IntoRaw};
use ::Result;
use ::event::{EventFutures, EventHandlers};
use ::window::{
    Bridge, BridgeKeepAlive, ClassKey,
    MessageHandler, MessageResult,
    MouseEventArgs,
    WindowState, WindowUserData,
    wnd_proc,
};

macro_rules! wnd {
    ($self_:expr) => {
        match $self_.bridge.get_wnd() {
            Some(wnd) => wnd,
            None => panic!("native window has been destroyed"),
        }
    };
}

macro_rules! try_wnd {
    ($self_:expr) => {
        match $self_.bridge.get_wnd() {
            Some(wnd) => wnd,
            None => return ::other_error("native window has been destroyed"),
        }
    };
}

pub struct HelloWindow {
    bridge: Rc<Bridge>,
    destroyed_futures: EventFutures<()>,
    mouse_event: EventHandlers<MouseEventArgs, ()>,
}

impl HelloWindow {
    pub fn new() -> Result<Rc<Self>> {
        trace!("HelloWindow::new");

        let class = ClassKey::new()
            .wnd_proc(wnd_proc)
            .style(wui::wnd_class_style::HRedraw | wui::wnd_class_style::VRedraw)
            .get_class()?;

        let (bridge, state) = Bridge::new(|bridge| {
            Ok(HelloWindow {
                bridge: bridge.clone(),
                destroyed_futures: EventFutures::new(),
                mouse_event: EventHandlers::new(),
            })
        })?;
        let keep_alive = BridgeKeepAlive::new(Rc::downgrade(&bridge));
        let wud = Box::new(WindowUserData::new(bridge));
        let wud_ptr = Box::into_raw(wud);

        let wnd = try!(wui::Wnd::new()
            .class_name(&*class)
            .window_name("Hello, World!")
            .style(wui::wnd_style::OverlappedWindow)
            .width(320).height(240)
            .param(wud_ptr)
            .create());

        let _ = wnd.into_raw();

        Gui::with_mut(|gui| gui.register_keep_alive(keep_alive));
        Ok(state)
    }

    pub fn keep_alive(&self, keep_alive: bool) {
        self.bridge.state_keep_alive(keep_alive)
    }

    pub fn set_caption(&self, caption: &str) -> Result<()> {
        let wnd = try_wnd!(self);
        wui::set_window_text(wnd, caption)
    }

    pub fn show(&self, cmd_show: wui::Show) -> Result<wui::WasVisible> {
        let wnd = try_wnd!(self);
        Ok(wui::show_window(wnd, cmd_show))
    }

    pub fn update(&self) -> Result<()> {
        let wnd = try_wnd!(self);
        wui::update_window(wnd)
    }

    pub fn on_destroyed_futures(&self) -> &EventFutures<()> {
        &self.destroyed_futures
    }

    pub fn on_mouse_event(&self) -> &EventHandlers<MouseEventArgs, ()> {
        &self.mouse_event
    }
}

impl Drop for HelloWindow {
    fn drop(&mut self) {
        trace!("HelloWindow::Drop");
        if let Some(wnd) = self.bridge.get_wnd() {
            let _ = wui::destroy_window(wnd);
        }
    }
}

impl WindowState for HelloWindow {
    fn as_any(&self) -> &::std::any::Any { self }
}

impl MessageHandler for HelloWindow {
    fn on_destroy(&self) -> Result<MessageResult<()>> {
        self.destroyed_futures.invoke(());
        Ok(MessageResult::Handled(()))
    }

    fn on_mouse_event(&self, args: &MouseEventArgs) -> Result<MessageResult<()>> {
        self.mouse_event.invoke(args.clone())
    }

    fn on_paint(&self, ps: &wui::WndPaint) -> Result<MessageResult<()>> {
        let wnd = try_wnd!(self);
        let dc = &ps.dc();
        let cr = wui::get_client_rect(&wnd)?;
        wui::rectangle(dc, cr.left, cr.top, cr.right, cr.bottom)?;
        wui::text_out(dc, 10, 10, "Hello, World!")?;
        Ok(MessageResult::Handled(()))
    }
}
