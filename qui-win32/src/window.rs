use std::any::Any;
use std::cell::{Cell, RefCell, UnsafeCell};
use std::collections::HashMap;
use std::io;
use std::mem;
use std::ptr;
use std::rc::{self, Rc};
use winapi::{self, HWND, LPVOID, LRESULT};
use wui;
use util::{Alive, FlexRc};
use ::Result;
use ::other_error;

/**
This type acts as the connection between a native window and the Rust-side state corresponding to said native window.

In addition, it helps control the lifetime of the native window: since we cannot *directly* refcount a native window, we instead refcount the bridge, and trigger destruction of the native window when the bridge is dropped.
*/
pub struct Bridge {
    /**
    Handle to the native window.

    This will be `null` in two situations:

    1. The native window has not yet processed its initial `WM_CREATE` message.

    2. The native window has been/is in the process of being destroyed (*i.e.* during or after `WM_DESTROY`).
    */
    wnd: Cell<HWND>,

    /**
    Reference to the corresponding window state object.
    */
    state: UnsafeCell<FlexRc<WindowState>>,
}

impl Bridge {
    pub fn new<State, F>(mk_state: F) -> Result<(Rc<Bridge>, Rc<State>)>
    where
        State: 'static + WindowState,
        F: FnOnce(&Rc<Bridge>) -> Result<State>,
    {
        // Construct bridge with temporary, dummy state and no handle.
        let dummy = FlexRc::from(Rc::new(DummyState) as Rc<WindowState>);
        let bridge = Rc::new(Bridge {
            wnd: ptr::null_mut().into(),
            state: dummy.into(),
        });

        // Get the *real* state constructed.
        let state = Rc::new(mk_state(&bridge)?);

        /*
        # Justification

        **Can anyone else be mutating `bridge.state`?**
        No.  First, they'd need to be using `unsafe`.  Secondly, they'd have to be doing so from another thread.
        */
        unsafe {
            let state = state.clone() as Rc<WindowState>;
            let state_flex = FlexRc::from(Rc::downgrade(&state));
            *bridge.state.get() = state_flex;
        }

        Ok((bridge, state))
    }

    pub fn get_wnd(&self) -> Option<HWND> {
        match self.wnd.get() {
            ptr if ptr.is_null() => None,
            ptr => Some(ptr),
        }
    }

    pub fn get_state(&self) -> Option<Rc<WindowState>> {
        unsafe {
            (*self.state.get()).upgraded()
        }
    }

    pub fn get_state_concrete<State>(&self) -> Rc<State>
    where State: WindowState {
        unsafe {
            // TODO: skip checks in release builds?
            let ws_rc = self.get_state().unwrap();
            assert!(ws_rc.as_any().is::<State>());
            (*mem::transmute::<_, &Rc<State>>(&ws_rc)).clone()
        }
    }

    pub fn state_keep_alive(&self, keep_alive: bool) {
        unsafe {
            if keep_alive {
                let (strong, _) = (*self.state.get()).upgraded_flex();
                *self.state.get() = strong;
            } else {
                let weak = (*self.state.get()).downgraded_flex();
                *self.state.get() = weak;
            }
        }
    }
}

impl Drop for Bridge {
    fn drop(&mut self) {
        trace!("Bridge::drop: dropping for {:p}", self.wnd.get());
    }
}

/**
This type is used to monitor a window bridge for alive-ness.

Specifically, it indicates that it is alive so long as the corresponding native window exists.
*/
pub struct BridgeKeepAlive(rc::Weak<Bridge>);

impl BridgeKeepAlive {
    pub fn new(bridge: rc::Weak<Bridge>) -> Self {
        BridgeKeepAlive(bridge)
    }
}

impl Alive for BridgeKeepAlive {
    fn is_alive(&self) -> bool {
        self.0.upgrade()
            .map(|bridge| !bridge.wnd.get().is_null())
            .unwrap_or(false)
    }
}

pub trait WindowState: MessageHandler + Any {
    fn as_any(&self) -> &Any;

    fn on_message(&self, msg: &wui::WndProcArgs) -> Result<MessageResult<LRESULT>> {
        self.dispatch_message(msg)
    }
}

pub trait MessageHandler {
    fn dispatch_message(&self, msg: &wui::WndProcArgs) -> Result<MessageResult<LRESULT>> {
        macro_rules! xb {
            () => {
                match (msg.w_param >> 16) & 0b11 {
                    1 => MouseButton::Other1,
                    2 => MouseButton::Other2,
                    _ => {
                        // Uh... umm... RUN AWAY!
                        return Ok(MessageResult::CallDefault);
                    }
                }
            };
        }

        macro_rules! me {
            ($event:ident) => {
                self
                    .on_mouse_event(&MouseEventArgs::new(
                        MouseEvent::$event,
                        msg.w_param, msg.l_param
                    ))
                    .map_handled(|_| 0)
            };

            ($event:ident(Other)) => {
                self
                    .on_mouse_event(&MouseEventArgs::new(
                        MouseEvent::$event(xb!()),
                        msg.w_param, msg.l_param
                    ))
                    .map_handled(|_| 0)
            };

            ($event:ident($button:ident)) => {
                self.on_mouse_event(&MouseEventArgs::new(
                        MouseEvent::$event(MouseButton::$button),
                        msg.w_param, msg.l_param
                    ))
                    .map_handled(|_| 0)
            };
        }

        match msg.message {
            winapi::WM_DESTROY => {
                self.on_destroy()
                    .map_handled(|_| 0)
            },

            winapi::WM_PAINT => {
                let ps = wui::WndPaint::begin_paint(msg.wnd)?;
                self.on_paint(&ps)
                    .map_handled(|_| 0)
            },

            // Mouse events
            // winapi::WM_CAPTURECHANGED => Ok(MessageResult::CallDefault),
            winapi::WM_LBUTTONDBLCLK => me!(DoubleDown(Left)),
            winapi::WM_LBUTTONDOWN => me!(Down(Left)),
            winapi::WM_LBUTTONUP => me!(Up(Left)),
            winapi::WM_MBUTTONDBLCLK => me!(DoubleDown(Middle)),
            winapi::WM_MBUTTONDOWN => me!(Down(Middle)),
            winapi::WM_MBUTTONUP => me!(Up(Middle)),
            // winapi::WM_MOUSEACTIVATE => Ok(MessageResult::CallDefault),
            // winapi::WM_MOUSEHOVER => Ok(MessageResult::CallDefault),
            // winapi::WM_MOUSEHWHEEL => Ok(MessageResult::CallDefault),
            // winapi::WM_MOUSELEAVE => Ok(MessageResult::CallDefault),
            winapi::WM_MOUSEMOVE => me!(Move),
            // winapi::WM_MOUSEWHEEL => Ok(MessageResult::CallDefault),
            // winapi::WM_NCHITTEST => Ok(MessageResult::CallDefault),
            // winapi::WM_NCLBUTTONDBLCLK => Ok(MessageResult::CallDefault),
            // winapi::WM_NCLBUTTONDOWN => Ok(MessageResult::CallDefault),
            // winapi::WM_NCLBUTTONUP => Ok(MessageResult::CallDefault),
            // winapi::WM_NCMBUTTONDBLCLK => Ok(MessageResult::CallDefault),
            // winapi::WM_NCMBUTTONDOWN => Ok(MessageResult::CallDefault),
            // winapi::WM_NCMBUTTONUP => Ok(MessageResult::CallDefault),
            // winapi::WM_NCMOUSEHOVER => Ok(MessageResult::CallDefault),
            // winapi::WM_NCMOUSELEAVE => Ok(MessageResult::CallDefault),
            // winapi::WM_NCMOUSEMOVE => Ok(MessageResult::CallDefault),
            // winapi::WM_NCRBUTTONDBLCLK => Ok(MessageResult::CallDefault),
            // winapi::WM_NCRBUTTONDOWN => Ok(MessageResult::CallDefault),
            // winapi::WM_NCRBUTTONUP => Ok(MessageResult::CallDefault),
            // winapi::WM_NCXBUTTONDBLCLK => Ok(MessageResult::CallDefault),
            // winapi::WM_NCXBUTTONDOWN => Ok(MessageResult::CallDefault),
            // winapi::WM_NCXBUTTONUP => Ok(MessageResult::CallDefault),
            winapi::WM_RBUTTONDBLCLK => me!(DoubleDown(Right)),
            winapi::WM_RBUTTONDOWN => me!(Down(Right)),
            winapi::WM_RBUTTONUP => me!(Up(Right)),
            winapi::WM_XBUTTONDBLCLK => me!(DoubleDown(Other)),
            winapi::WM_XBUTTONDOWN => me!(Down(Other)),
            winapi::WM_XBUTTONUP => me!(Up(Other)),

            _ => Ok(MessageResult::CallDefault)
        }
    }

    fn on_destroy(&self) -> Result<MessageResult<()>> {
        Ok(MessageResult::CallDefault)
    }

    fn on_mouse_event(&self, args: &MouseEventArgs) -> Result<MessageResult<()>> {
        drop(args);
        Ok(MessageResult::CallDefault)
    }

    fn on_paint(&self, ps: &wui::WndPaint) -> Result<MessageResult<()>> {
        drop(ps);
        Ok(MessageResult::CallDefault)
    }
}

#[derive(Clone, Debug)]
pub struct MouseEventArgs {
    pub event: MouseEvent,
    state: (), // translate wparam
    pub x: i32,
    pub y: i32,
}

impl MouseEventArgs {
    fn new(event: MouseEvent, w_param: winapi::WPARAM, l_param: winapi::LPARAM) -> MouseEventArgs {
        drop(w_param);
        MouseEventArgs {
            event: event,
            state: (),
            x: ((l_param as u32) >> 0) as u16 as i16 as i32,
            y: ((l_param as u32) >> 16) as u16 as i16 as i32,
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum MouseEvent {
    Down(MouseButton),
    Up(MouseButton),
    DoubleDown(MouseButton),
    Move,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum MouseButton {
    Left,
    Right,
    Middle,
    Other1,
    Other2,
}

pub enum MessageResult<T> {
    Handled(T),
    CallDefault,
}

impl<T> MessageResult<T> {
    pub fn map_handled<F, R>(self, f: F) -> MessageResult<R>
    where F: FnOnce(T) -> R {
        match self {
            MessageResult::Handled(v) => MessageResult::Handled(f(v)),
            MessageResult::CallDefault => MessageResult::CallDefault,
        }
    }
}

trait ResultMessageResultExt<T>: Sized {
    fn map_handled<F, R>(self, f: F) -> Result<MessageResult<R>>
    where F: FnOnce(T) -> R;
}

impl<T> ResultMessageResultExt<T> for Result<MessageResult<T>> {
    fn map_handled<F, R>(self, f: F) -> Result<MessageResult<R>>
    where F: FnOnce(T) -> R {
        self.map(|v| v.map_handled(f))
    }
}

pub const WUD_MAGIC: &'static [u8; 8] = b"QUINDOWS";

pub struct WindowUserData {
    magic: [u8; 8], // "QUINDOWS"
    pub bridge: Rc<Bridge>,
}

impl WindowUserData {
    pub fn new(bridge: Rc<Bridge>) -> Self {
        WindowUserData {
            magic: *WUD_MAGIC,
            bridge: bridge,
        }
    }

    pub unsafe fn try_from_lpvoid(ptr: &LPVOID) -> Result<&Self> {
        if ptr.is_null() {
            return other_error("cannot create WindowUserData from null ptr");
        }
        let ptr = &*(*ptr as *const WindowUserData);
        if &ptr.magic != WUD_MAGIC {
            return other_error(format!("WindowUserData magic mismatch: `{:?}` != `{:?}`",
                &ptr.magic[..], &WUD_MAGIC[..]));
        }
        Ok(ptr)
    }

    pub unsafe fn try_from_ptr(ptr: &*const WindowUserData) -> Result<&Self> {
        if ptr.is_null() {
            return other_error("cannot create WindowUserData from null ptr");
        }
        let ptr = &**ptr;
        if &ptr.magic != WUD_MAGIC {
            return other_error(format!("WindowUserData magic mismatch: `{:?}` != `{:?}`",
                &ptr.magic[..], &WUD_MAGIC[..]));
        }
        Ok(ptr)
    }
}

wnd_proc! { pub unsafe extern "system" fn wnd_proc |> try_wnd_proc; }

fn try_wnd_proc(args: wui::WndProcArgs)
-> wui::WndProcResult<io::Error> {
    use winapi::*;
    use ::{other_error, /*other_err*/};

    // NOTE: this is really, *really* noisy.
    // trace!("try_wnd_proc: {}", args);

    match args.message {
        WM_CREATE => unsafe {
            info!("try_wnd_proc: WM_CREATE {:p}", args.wnd);
            /*
            The major task here is to set up the window's user data.  This is also the earliest point at which we can know what our window's handle is.

            To start, we pull the pointer (which is really a `Box<WUD>`) out from the create parameters.
            */
            let l_param_ptr = args.l_param as *mut CREATESTRUCTW;
            if l_param_ptr.is_null() {
                return other_error("got null *mut CREATESTRUCTW in WM_CREATE");
            }
            let l_param = &mut *l_param_ptr;

            {
                // Continue until we have a WUD we can use.
                let wud_ptr = l_param.lpCreateParams;
                if wud_ptr.is_null() {
                    return other_error("got null *mut WindowUserData in WM_CREATE");
                }
                let wud = WindowUserData::try_from_lpvoid(&wud_ptr)?;

                /*
                Ok, it's *definitely* a WUD.  Before we do anything else, write the pointer into the window's user data slot.  This is what links the native window to our state.
                */
                try!(wui::set_window_long_ptr(args.wnd, GWLP_USERDATA, wud_ptr));

                // We can now set the handle on the bridge.
                wud.bridge.wnd.set(args.wnd);

                // Let the WUD do its thing.
                if let Some(state) = wud.bridge.get_state() {
                    match state.on_message(&args)? {
                        MessageResult::Handled(lr) => Ok(Some(lr)),
                        MessageResult::CallDefault => Ok(wui::def_window_proc(args)),
                    }
                } else {
                    Ok(Some(0))
                }
            }
        },

        WM_DESTROY => unsafe {
            info!("try_wnd_proc: WM_DESTROY {:p}", args.wnd);
            wui_ok_or_warn! {
                {
                    /*
                    This first block is here so that the window state's `on_destroy` handler has a chance to inspect the native window *before* it gets destroyed.  We don't want to break the link between the two *just* yet...
                    */
                    let wud_ptr: *const WindowUserData = wui::get_window_long_ptr(args.wnd, GWLP_USERDATA)?;
                    let wud = WindowUserData::try_from_ptr(&wud_ptr)?;

                    // Notify the WUD before we continue.
                    if let Some(state) = wud.bridge.get_state() {
                        // Note that whatever the WUD *says*, we just ignore it and keep going.
                        match state.on_message(&args)? {
                            MessageResult::Handled(_) => (),
                            MessageResult::CallDefault => (),
                        }
                    }
                }

                /*
                With that previous block done, we can now *actually* start destroying the bridge between the two.
                */
                let wud_ptr: *const WindowUserData = wui::set_window_long_ptr(args.wnd, GWLP_USERDATA, ::std::ptr::null_mut())?;
                {
                    let wud = WindowUserData::try_from_ptr(&wud_ptr)?;
                    wud.bridge.wnd.set(ptr::null_mut());
                }

                /*
                Drop the `WindowUserData`.

                This *should* be OK because we just erased the pointer in the window itself.  MSDN doesn't specify whether or not this is atomic, but it *should* be OK.  There's only so un-atomic it can be.
                */
                let wud_ptr = wud_ptr as *mut WindowUserData;
                let wud = Box::from_raw(wud_ptr);
                drop(wud);

                Ok(())
            }
            Ok(Some(0))
        },

        _ => unsafe {
            let wud_ptr: *const WindowUserData = wui::get_window_long_ptr(args.wnd, GWLP_USERDATA)?;
            let wud = match WindowUserData::try_from_ptr(&wud_ptr) {
                Ok(wud) => wud,
                Err(_) => {
                    /*
                    The problem here is that there are various messages we can receive *before* the window has been created.  From the OS itself.

                    *Why.*

                    Plus, insofar as I know, there's no reason Microsoft couldn't start sending *other* messages, so we need to be careful here.

                    So, if we get a message when the WUD pointer is invalid, we'll fall back on just calling the default window procedure, and hoping for the best.
                    */
                    return Ok(wui::def_window_proc(args));
                }
            };

            // Notify WUD.
            if let Some(state) = wud.bridge.get_state() {
                match state.on_message(&args)? {
                    MessageResult::Handled(lr) => Ok(Some(lr)),
                    MessageResult::CallDefault => Ok(wui::def_window_proc(args)),
                }
            } else {
                Ok(wui::def_window_proc(args))
            }
        },
    }
}

/**
This type is used to define "uniqueness" of window classes.
*/
#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub struct ClassKey {
    wnd_proc: Option<usize>,
    style: Option<wui::WndClassStyle>,
}

impl ClassKey {
    pub fn new() -> ClassKey {
        ClassKey {
            wnd_proc: None,
            style: None,
        }
    }

    pub fn wnd_proc(self, v: wui::WndProcRef) -> Self {
        unsafe {
            ClassKey {
                wnd_proc: Some(mem::transmute(v)),
                ..self
            }
        }
    }

    pub fn style(self, v: wui::WndClassStyle) -> Self {
        ClassKey {
            style: Some(v),
            ..self
        }
    }

    pub fn get_class(self) -> Result<Rc<wui::WndClass>> {
        use std::collections::hash_map::Entry::*;
        CLASSES.with(move |classes| {
            match classes.borrow_mut().entry(self.clone()) {
                Occupied(e) => Ok(e.get().clone()),
                Vacant(e) => {
                    // Construct the class.
                    let class = wui::WndClass::new()
                        .class_name(&self.compute_name())
                        .wnd_proc(unsafe { mem::transmute(self.wnd_proc.unwrap()) })
                        .instance(try!(wui::get_module_handle(None)))
                        .style_opt(self.style)
                        .register()
                        .map(Rc::new)?;
                    Ok(e.insert(class).clone())
                },
            }
        })
    }

    /**
    Computes the unique string name for this window class.
    */
    fn compute_name(&self) -> String {
        format!(
            "qui-window-{wnd_proc:x}-{style:?}",
            wnd_proc = self.wnd_proc.expect("ClassKey::wnd_proc not called") as usize,
            style = self.style,
        )
    }
}

thread_local! {
    static CLASSES: RefCell<HashMap<ClassKey, Rc<wui::WndClass>>> = {
        RefCell::new(HashMap::new())
    };
}

struct DummyState;

macro_rules! dnuds {
    () => {
        panic!("DummyState must not be used");
    };
}

impl WindowState for DummyState {
    fn as_any(&self) -> &Any { dnuds!(); }
    fn on_message(&self, _: &wui::WndProcArgs) -> Result<MessageResult<LRESULT>> { dnuds!(); }
}

impl MessageHandler for DummyState {}
