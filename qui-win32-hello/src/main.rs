#![feature(question_mark)]
#[macro_use] extern crate log;
extern crate env_logger;
extern crate futures;
extern crate qui_win32;
extern crate user32;
extern crate wui;
extern crate winapi;

use std::time::Duration;
use futures::Future;
use futures::stream::Stream;
use qui_win32::ext::{FutureTaskExt, MouseEventStreamExt};
use qui_win32::gui::{self, Gui};
use qui_win32::window::{MessageResult, MouseButton, MouseEventArgs};

fn main() {
    env_logger::init().unwrap();
    info!("main: starting up");

    trace!("main: initialising Gui");
    Gui::init(|| {
        trace!("main: creating side task 1");
        futures::done(Ok(()))
            .and_then(|()| gui::Wait::new(Duration::from_secs(1)))
            .map(|_| {
                println!("+++ Side task 1 complete! +++");
            })
            .spawn_task();

        trace!("main: creating side task 2");
        futures::done(Ok(()))
            .and_then(|()| gui::Wait::new(Duration::from_secs(3)))
            .map(|_| {
                println!("+++ Side task 2 complete! +++");
            })
            .spawn_task();

        trace!("main: creating window");
        let hello = qui_win32::hello::HelloWindow::new()
            .expect("couldn't create hello window");

        let caption = "Oh, hai there!";
        hello.keep_alive(true);
        hello.set_caption(caption).unwrap();
        hello.show(wui::Show::ShowDefault).unwrap();
        hello.update().unwrap();

        trace!("main: creating event handlers");
        hello.on_destroyed_futures().future()
            .map(|_| {
                println!("--- Hello window destroyed.");
            })
            .spawn_task();

        hello.on_mouse_event().callback(|arg| {
            if arg.event != qui_win32::window::MouseEvent::Move {
                println!("--- Mouse event: {:?}", arg);
            }
            Ok(MessageResult::CallDefault)
        });

        let (set_click_cd, click_cd) = gui::countdown();

        hello.on_mouse_event().stream()
            .clicked(MouseButton::Left)
            .for_each({ let hello = hello.clone(); move |args| {
                let MouseEventArgs { x, y, .. } = args;
                hello.set_caption(&format!("Click at: ({}, {})", x, y))?;
                set_click_cd.set_or_reset(Duration::from_secs(2), ());
                Ok(())
            }})
            .spawn_task();

        hello.on_mouse_event().stream()
            .clicked(MouseButton::Right)
            .for_each({ move |_| {
                println!("~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~");
                Ok(())
            }})
            .spawn_task();

        click_cd
            .for_each({ let hello = hello.clone(); move |()| {
                hello.set_caption(caption)?;
                Ok(())
            }})
            .spawn_task();

        trace!("main: creating main future");
        // let fut = gui::AllKeepAliveDead::new()
        //     .and_then(|_| {
        //         println!("+++ Main task complete! +++");
        //         futures::done(Ok(()))
        //     });
        let fut = hello.on_destroyed_futures().future()
            .map(|_| {
                println!("+++ Main task complete! +++");
            });

        trace!("main: running future");
        let r = Gui::run(fut);

        info!("main: result: {:?}", r);
    }).expect("Gui::init failed");

    info!("main: shutting down");
}
