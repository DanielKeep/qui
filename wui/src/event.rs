use std::io;
use std::mem;
use std::ptr;
use kernel32;
use winapi::*;
use ::last_error;
use ::sync::SyncRights;
use ::traits::{AsRaw, IntoRaw};
use ::util::{TryDrop, WCString};

bitflags! {
    flags EventFlag, event_flag: DWORD {
        const InitialSet = 2 /*::winapi::CREATE_EVENT_INITIAL_SET*/,
        const ManualReset = 1 /*::winapi::CREATE_EVENT_MANUAL_RESET*/,
    }
}

pub struct Event(HANDLE);

impl Event {
    pub unsafe fn create_raw(
        event_attributes: LPSECURITY_ATTRIBUTES,
        name: LPCWSTR,
        flags: DWORD,
        desired_access: DWORD,
    ) -> io::Result<Self> {
        match kernel32::CreateEventExW(event_attributes, name, flags, desired_access) {
            v if v.is_null() => last_error(),
            v => Ok(Event(v))
        }
    }

    pub fn set(&self) -> io::Result<()> {
        unsafe {
            match kernel32::SetEvent(self.as_raw()) {
                0 => last_error(),
                _ => Ok(())
            }
        }
    }
}

impl AsRaw for Event {
    type Raw = HANDLE;

    fn as_raw(&self) -> Self::Raw {
        self.0
    }
}

impl IntoRaw for Event {
    fn into_raw(self) -> HANDLE {
        let raw = self.0;
        mem::forget(self);
        raw
    }
}

impl Drop for Event {
    fn drop(&mut self) {
        unsafe { self.try_drop_inner().unwrap() }
    }
}

impl TryDrop for Event {
    type Err = io::Error;

    unsafe fn try_drop_inner(&mut self) -> Result<(), Self::Err> {
        match kernel32::CloseHandle(self.0) {
            0 => last_error(),
            _ => Ok(())
        }
    }
}

pub struct EventBuilder {
    name: Option<WCString>,
    flags: Option<EventFlag>,
    desired_access: Option<SyncRights>,
}

impl EventBuilder {
    pub fn new() -> EventBuilder {
        EventBuilder {
            name: None,
            flags: None,
            desired_access: None,
        }
    }

    pub fn name(self, v: &str) -> Self {
        EventBuilder {
            name: Some(v.into()),
            ..self
        }
    }

    pub fn flags(self, v: EventFlag) -> Self {
        EventBuilder {
            flags: Some(v),
            ..self
        }
    }

    pub fn desired_access(self, v: SyncRights) -> Self {
        EventBuilder {
            desired_access: Some(v),
            ..self
        }
    }

    pub fn create(self) -> io::Result<Event> {
        unsafe {
            let event_attributes = ptr::null_mut();
            let name = self.name.as_ref().map(|s| s.as_ptr()).unwrap_or(ptr::null());
            let flags = self.flags.map(|v| v.bits()).unwrap_or(0);
            let desired_access = self.desired_access.expect("missing desired_access").bits();
            Event::create_raw(event_attributes, name, flags, desired_access)
        }
    }
}
