use std::io;
use std::ptr;
use ole32;
use winapi::*;
use ::other_error;

bitflags! {
    flags CoInit, co_init: DWORD {
        const ApartmentThreaded = ::winapi::COINIT_APARTMENTTHREADED,
        const MultiThreaded = ::winapi::COINIT_MULTITHREADED,
        const DisableOle1Dde = ::winapi::COINIT_DISABLE_OLE1DDE,
        const SpeedOverMemory = ::winapi::COINIT_SPEED_OVER_MEMORY,
    }
}

pub enum CoInitResult {
    Initialized,
    AlreadyInitialized,
}

pub fn co_initialize_ex(co_init: Option<CoInit>) -> io::Result<CoInitResult> {
    unsafe {
        let reserved = ptr::null_mut();
        let co_init = co_init.map(|v| v.bits()).unwrap_or(0);
        match ole32::CoInitializeEx(reserved, co_init) {
            S_OK => Ok(CoInitResult::Initialized),
            S_FALSE => Ok(CoInitResult::AlreadyInitialized),
            v => other_error(&format!("co_initialize_ex failed: 0x{:08x}", v)),
        }
    }
}

pub fn co_uninitialize() {
    unsafe {
        ole32::CoUninitialize();
    }
}

pub fn with_com<F, R>(co_init: Option<CoInit>, f: F) -> io::Result<R>
where F: FnOnce() -> R {
    try!(co_initialize_ex(co_init));
    let r = f();
    co_uninitialize();
    Ok(r)
}
