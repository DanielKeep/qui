use std::io;
use std::mem;
use gdi32;
use winapi::*;
use ::other_error;
use ::traits::AsRaw;

pub struct Dc(HDC);

impl Dc {
    pub fn from_ref(dc: &HDC) -> &Dc {
        unsafe {
            mem::transmute(dc)
        }
    }
}

impl AsRaw for Dc {
    type Raw = HDC;
    fn as_raw(&self) -> Self::Raw {
        self.0
    }
}

impl AsRaw for HDC {
    type Raw = Self;
    fn as_raw(&self) -> Self::Raw {
        *self
    }
}

pub fn rectangle<Dc>(dc: &Dc, left: i32, top: i32, right: i32, bottom: i32) -> io::Result<()>
where Dc: AsRaw<Raw=HDC> {
    unsafe {
        let dc = dc.as_raw();
        match gdi32::Rectangle(dc, left, top, right, bottom) {
            0 => other_error("Rectangle failed"),
            _ => Ok(())
        }
    }
}
