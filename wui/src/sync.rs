use std::io;
use std::iter::IntoIterator;
use std::ptr;
use std::time::Duration;
use winapi;
use user32;
use ::{last_error, other_error};

bitflags! {
    flags SyncRights, sync_rights: ::winapi::DWORD {
        const Delete = ::winapi::DELETE,
        const ReadControl = ::winapi::READ_CONTROL,
        const Synchronize = ::winapi::SYNCHRONIZE,
        const WriteDac = ::winapi::WRITE_DAC,
        const WriteOwner = ::winapi::WRITE_OWNER,

        const EventAllAccess = 0x1F0003 /*::winapi::EVENT_ALL_ACCESS*/,
        const EventModifyState = 0x0002 /*::winapi::EVENT_MODIFY_STATE*/,

        const MutexAllAccess = 0x1F0001 /*::winapi::MUTEX_ALL_ACCESS*/,
        const MutexModifyState = 0x0001 /*::winapi::MUTEX_MODIFY_STATE*/,

        const SemaphoreAllAccess = 0x1F0003 /*::winapi::SEMAPHORE_ALL_ACCESS*/,
        const SemaphoreModifyState = 0x0002 /*::winapi::SEMAPHORE_MODIFY_STATE*/,

        const TimerAllAccess = 0x1F0003 /*::winapi::TIMER_ALL_ACCESS*/,
        const TimerModifyState = 0x0002 /*::winapi::TIMER_MODIFY_STATE*/,
        const TimerQueryState = 0x0001 /*::winapi::TIMER_QUERY_STATE*/,

        const AccessSystemSecurity = ::winapi::ACCESS_SYSTEM_SECURITY,
    }
}

pub fn msg_wait_for_multiple_objects_ex<HandleIt>(
    handles: HandleIt,
    timeout: Option<Duration>,
    wake_mask: winapi::DWORD, // QS_*
    flags: winapi::DWORD, // 0 | MWMO_*
) -> io::Result<MsgWaitResult>
where HandleIt: IntoIterator<Item=winapi::HANDLE> {
    unsafe {
        const MAX_WAIT: usize = (winapi::MAXIMUM_WAIT_OBJECTS - 1) as usize;
        let mut handles_arr = [ptr::null_mut(); MAX_WAIT];
        let mut handles_cnt = 0;
        for (i, handle) in handles.into_iter().enumerate() {
            if i < handles_arr.len() {
                handles_arr[i] = handle;
                handles_cnt += 1;
            } else {
                return other_error("tried to wait on too many objects");
            }
        }

        let handles = &handles_arr[..handles_cnt];

        let (handles, count) = (handles, handles.len() as winapi::DWORD);
        let timeout = timeout
            .map(|dur| {
                use conv::prelude::*;
                dur.as_secs().checked_mul(1_000)
                    .and_then(|ms| ms.checked_add((dur.subsec_nanos() / 1_000_000) as u64))
                    .and_then(|ms| ms.value_as::<winapi::DWORD>().ok())
                    .unwrap_or(::std::u32::MAX)
            })
            .unwrap_or(winapi::INFINITE);
        let wake_mask = wake_mask;
        let flags = flags;

        if (flags & winapi::MWMO_WAITALL) != 0 {
            panic!("using MWMO_WAITALL is considered a bug: <https://blogs.msdn.microsoft.com/oldnewthing/20060127-17/?p=32493/>");
        }

        let res = user32::MsgWaitForMultipleObjectsEx(count, handles.as_ptr(), timeout, wake_mask, flags);

        if res == winapi::WAIT_FAILED {
            return last_error();
        }

        if winapi::WAIT_OBJECT_0 <= res && res < winapi::WAIT_OBJECT_0 + count {
            let idx = res - winapi::WAIT_OBJECT_0;
            return Ok(MsgWaitResult::HandleReady(idx as usize));
        }

        if res == winapi::WAIT_OBJECT_0 + count {
            return Ok(MsgWaitResult::Message);
        }

        if winapi::WAIT_ABANDONED_0 <= res && res < winapi::WAIT_ABANDONED_0 + count {
            let idx = res - winapi::WAIT_ABANDONED_0;
            return Ok(MsgWaitResult::MutexAbandoned(idx as usize));
        }

        if res == winapi::WAIT_IO_COMPLETION {
            return Ok(MsgWaitResult::Apc);
        }

        if res == winapi::WAIT_TIMEOUT {
            return Ok(MsgWaitResult::Timeout);
        }

        // Uhhh... I'm not sure I want to make this an actual *error*, for the sake of resilience.
        Ok(MsgWaitResult::Unknown)
    }
}

#[derive(Debug)]
pub enum MsgWaitResult {
    HandleReady(usize),
    Message,
    MutexAbandoned(usize),
    Apc,
    Timeout,
    Unknown,
}
