use std::convert::AsRef;
use std::ffi::OsStr;
use std::io;
use std::process::Command;
use ::io_err;
use self::ToolchainKind::*;
use self::ToolchainArch::*;

macro_rules! try_opt {
    ($e:expr) => {
        match $e {
            Some(v) => v,
            None => return None,
        }
    };
}

macro_rules! try_some {
    ($e:expr) => {
        match $e {
            Some(v) => return Some(v),
            None => (),
        }
    };
}

#[derive(Eq, PartialEq, Debug)]
pub struct Toolchain {
    pub kind: ToolchainKind,
    pub arch: ToolchainArch,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum ToolchainKind {
    MinGW,
    Msvc,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum ToolchainArch {
    X86,
    X64,
}

impl Toolchain {
    pub fn from_target(target: &str) -> Option<Toolchain> {
        let kind = if target.ends_with("-gnu") {
            MinGW
        } else if target.ends_with("-msvc") {
            Msvc
        } else {
            return None;
        };

        let arch = if target.starts_with("i686-") {
            X86
        } else if target.starts_with("x86_64-") {
            X64
        } else {
            return None;
        };

        Some(Toolchain {
            kind: kind,
            arch: arch,
        })
    }

    pub fn res_ext(&self) -> &'static str {
        match self.kind {
            MinGW => "o",

            // Yes, I know this is wrong.  See elsewhere about cargo and `LINK`.
            Msvc => "lib",
        }
    }

    pub fn rc_command<Input, Output>(&self, input: Input, output: Output) -> io::Result<Command>
    where
        Input: AsRef<OsStr>,
        Output: AsRef<OsStr>,
    {
        match self.kind {
            MinGW => {
                let cmd = try!(mingw::find_bin("windres.exe", self.arch)
                    .ok_or_else(|| io_err("could not locate windres.exe")));
                let mut cmd = Command::new(&cmd);
                cmd.arg("-J").arg("rc")
                    .arg("-i").arg(input)
                    .arg("-O").arg("coff")
                    .arg("-o").arg(output)
                    ;
                Ok(cmd)
            },
            Msvc => {
                let cmd = try!(msvc::find_sdk_tool("rc.exe", self.arch)
                    .ok_or_else(|| io_err("could not locate rc.exe")));
                let mut cmd = Command::new(&cmd);
                cmd.arg("/nologo")
                    .arg("/fo").arg(output)
                    .arg(input);
                Ok(cmd)
            },
        }
    }
}

mod mingw {
    use std::path::PathBuf;
    use super::ToolchainArch;
    use super::util;

    pub fn find_bin(name: &str, arch: ToolchainArch) -> Option<PathBuf> {
        let _ = arch;

        // Fallback.
        util::which(name)
    }
}

mod msvc {
    use std::path::PathBuf;
    use super::ToolchainArch;
    use super::util;

    pub fn find_sdk_tool(name: &str, arch: ToolchainArch) -> Option<PathBuf> {
        let try_dir = |mut root: PathBuf| -> Option<PathBuf> {
            root.push("bin");
            root.push(match arch {
                ToolchainArch::X86 => "x86",
                ToolchainArch::X64 => "x64",
            });

            if !root.is_dir() {
                return None;
            }

            root.push(name);
            if root.is_file() {
                Some(root)
            } else {
                None
            }
        };

        try_some!((|| {
            get_sdk10_dir().and_then(&try_dir)
                .or_else(|| get_sdk81_dir().and_then(&try_dir))
                .or_else(|| get_sdk8_dir().and_then(&try_dir))
        })());

        // Fallback.
        util::which(name)
    }

    // Copyright 2015 The Rust Project Developers. See the COPYRIGHT
    // file at the top-level directory of this distribution and at
    // http://rust-lang.org/COPYRIGHT.
    //
    // Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
    // http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
    // <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
    // option. This file may not be copied, modified, or distributed
    // except according to those terms.

    // MSVC-specific logic for linkers and such.
    //
    // This module contains a cross-platform interface but has a blank unix
    // implementation. The Windows implementation builds on top of Windows native
    // libraries (reading registry keys), so it otherwise wouldn't link on unix.
    //
    // Note that we don't have much special logic for finding the system linker on
    // any other platforms, so it may seem a little odd to single out MSVC to have
    // a good deal of code just to find the linker. Unlike Unix systems, however,
    // the MSVC linker is not in the system PATH by default. It also additionally
    // needs a few environment variables or command line flags to be able to link
    // against system libraries.
    //
    // In order to have a nice smooth experience on Windows, the logic in this file
    // is here to find the MSVC linker and set it up in the default configuration
    // one would need to set up anyway. This means that the Rust compiler can be
    // run not only in the developer shells of MSVC but also the standard cmd.exe
    // shell or MSYS shells.
    //
    // As a high-level note, all logic in this module for looking up various
    // paths/files is based on Microsoft's logic in their vcvars bat files, but
    // comments can also be found below leading through the various code paths.

    use ::registry::LOCAL_MACHINE;

    // Vcvars finds the correct version of the Windows 10 SDK by looking
    // for the include `um\Windows.h` because sometimes a given version will
    // only have UCRT bits without the rest of the SDK. Since we only care about
    // libraries and not includes, we instead look for `um\x64\kernel32.lib`.
    // Since the 32-bit and 64-bit libraries are always installed together we
    // only need to bother checking x64, making this code a tiny bit simpler.
    // Like we do for the Universal CRT, we sort the possibilities
    // asciibetically to find the newest one as that is what vcvars does.
    fn get_sdk10_dir() -> Option<PathBuf> {
        let key = try_opt!(LOCAL_MACHINE
            .open(r"SOFTWARE\Microsoft\Microsoft SDKs\Windows\v10.0".as_ref()).ok());
        let root = try_opt!(key.query_str("InstallationFolder").ok());
        Some(PathBuf::from(root))
    }

    fn get_sdk81_dir() -> Option<PathBuf> {
        let key = try_opt!(LOCAL_MACHINE
            .open(r"SOFTWARE\Microsoft\Microsoft SDKs\Windows\v8.1".as_ref()).ok());
        let root = try_opt!(key.query_str("InstallationFolder").ok());
        Some(PathBuf::from(root))
    }

    fn get_sdk8_dir() -> Option<PathBuf> {
        let key = try_opt!(LOCAL_MACHINE
            .open(r"SOFTWARE\Microsoft\Microsoft SDKs\Windows\v8.0".as_ref()).ok());
        let root = try_opt!(key.query_str("InstallationFolder").ok());
        Some(PathBuf::from(root))
    }
}

mod util {
    use std::env;
    use std::path::PathBuf;

    pub fn which(name: &str) -> Option<PathBuf> {
        let paths = try_opt!(env::var_os("PATH"));
        for path in env::split_paths(&paths) {
            let mut path = PathBuf::from(path);
            path.push(name);
            if path.is_file() {
                return Some(path);
            }
        }
        None
    }
}
